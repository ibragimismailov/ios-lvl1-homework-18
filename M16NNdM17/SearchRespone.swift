//
//  SearchRespone.swift
//  M16NNdM17
//
//  Created by Abraam on 10.01.2022.
//

import Foundation
struct SearchRespone:Decodable {
    var searchType:String
    var expression:String
    var results:[MovieResult]
}
struct MovieResult:Decodable {
    var id:String
    var resultType:String
    var image:String
    var title:String
    var description:String
}
struct ErrorMessage {
    var errorMessage:String
}
