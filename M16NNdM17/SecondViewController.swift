//
//  SecondViewController.swift
//  M16NNdM17
//
//  Created by Abraam on 28.12.2021.
//


import UIKit
import SnapKit


class SecondViewController: UIViewController {
   
    private  let imageURLs = [
        "https://www.kino-teatr.ru/movie/posters/big/6/55826.jpg",
      "https://avatars.mds.yandex.net/i?id=daa4cbb0aa1ea1a086309df5c6113034-4518693-images-thumbs&n=13",
    "https://avatars.mds.yandex.net/i?id=dcce024b7562b61f2d52c5f3ecce58d9-5645067-images-thumbs&n=13",
        "https://www.hi-fi.ru/upload/medialibrary/bd3/bd3bc43e17443c16b006441045bc887d.jpg",
        "https://cdn.fishki.net/upload/post/2018/03/17/2540083/11-7.jpg"
        
    ]
    private var images = [Data]()
    private lazy var stackView:UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 20
        return stackView
    }()
    private lazy var dissmisbutton:UIButton = {
        let dissmis = UIButton()
        dissmis.setTitle("back", for: .normal)
        dissmis.setTitleColor(.white, for: .normal)
        dissmis.backgroundColor = .gray
        dissmis.layer.borderWidth = 1
        dissmis.layer.cornerRadius = 5
        dissmis.addTarget(self, action: #selector(backTo), for: .touchUpInside)
        
        return dissmis
    }()

 
    
    @objc private func backTo () {
        self.dismiss(animated: true, completion: nil)
    }
    private lazy var activityIndicator : UIActivityIndicatorView = {
        let acIdentifier = UIActivityIndicatorView()
        acIdentifier.style = .large
        return acIdentifier
    }()
   

    override func viewDidLoad() {
        view.backgroundColor = .white
        setupView()
        super.viewDidLoad()
        
//        asynUsual()
        asynGroup()
        //asynGroup2()
     
       
    }
    private func asynUsual() {
        print("asynUsual")
        for i in 0...4 {
            let url = URL(string: imageURLs[i])
            let request = URLRequest(url: url!)
            let task = URLSession.shared.dataTask(with: request){
                (data,response,error) -> Void in
                DispatchQueue.main.async {[weak self] in
                    guard let self = self else {return}
                    print("Added image\(i)")
                    self.loadImage(data: data!)
                }
            }
            task.resume()
            //activityIndicator.stopAnimating()
        }
    }
    
    private func asynGroup() {
        let group = DispatchGroup()
        for i in 0...4{
            group.enter()
            getURLString(imageUrl: URL(string: imageURLs[i])!,
                         runQueue: DispatchQueue.global(),
                         completionQueue: DispatchQueue.main)
            {resault,error in
                guard let image1 = resault else {return}
                self.images.append(image1)
                group.leave()
            }
        }
        group.notify(queue: DispatchQueue.main){[weak self] in
            guard let self = self else {return}
            self.activityIndicator.stopAnimating()
            self.stackView.removeArrangedSubview(self.activityIndicator)
            for i in 0...4 {
                self.loadImage(data: self.images[i])
            }
        }

    }
    private func setupView(){
        view.addSubview(stackView)
        view.addSubview(dissmisbutton)
        dissmisbutton.snp.makeConstraints { make in
            make.top.equalTo(view.snp_topMargin).inset(5)
            make.right.equalTo(view.snp_rightMargin).inset(300)
            make.left.equalTo(view.snp_leftMargin).inset(-5)
        }
        stackView.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottomMargin )
            make.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin)
            make.left.equalTo(view.snp_leftMargin).offset(-20)
            make.right.equalTo(view.snp_rightMargin).inset(-20)
           
        }
        stackView.addArrangedSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
 
    

}
private extension SecondViewController {
    func getURLString(
        imageUrl:URL,
        runQueue:DispatchQueue,
        completionQueue:DispatchQueue,
        completion:@escaping(Data?,Error?)->()
    ){
        runQueue.async {
            do {
              let data =   try Data(contentsOf: imageUrl)
                sleep(arc4random() % 3)
                completionQueue.async { completion(data, nil)
                }
            } catch let error {
                completionQueue.async {completion(nil, error)
                    
                }
            }
        }
        
    }
    func loadImage(data:Data){
        let view  = UIImageView(image: UIImage(data: data))
        view.contentMode  = .scaleAspectFit
        self.stackView.addArrangedSubview(view)
        
    }
}

