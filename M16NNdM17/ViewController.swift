//
//  ViewController.swift
//  M16NNdM17
//
//  Created by Abraam on 27.12.2021.
//


import UIKit

class ViewController: UIViewController {
private var collectionView:UICollectionView?
    let searchController = UISearchController(searchResultsController: nil)
var moviesList = [Movies]()
var studens = Student()
    
   
    
override func viewDidLoad() {
super.viewDidLoad()


navigationController?.navigationBar.prefersLargeTitles = true
let layout = UICollectionViewFlowLayout()
layout.scrollDirection = .vertical
layout.itemSize = CGSize(width: 177, height: 319)
collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    collectionView?.contentInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
guard let collectionView = collectionView else {
return
}
collectionView.dataSource = self
collectionView.delegate = self
   

let m1 = Movies(movieID: 1, moviePrice: 23.99, movieName: "interstellar ", movieImageName: "interstellar")
let m2 = Movies(movieID: 2, moviePrice: 33.99, movieName: "Titanic", movieImageName: "Titanic")
    let m3 = Movies(movieID: 1, moviePrice: 23.99, movieName: "Arrival ", movieImageName: "arrival")
    let m4 = Movies(movieID: 2, moviePrice: 33.99, movieName: "Inception", movieImageName: "inception")
    let m5 = Movies(movieID: 2, moviePrice: 33.99, movieName: "Lucy", movieImageName: "lucy")
moviesList.append(m1)
moviesList.append(m2)
moviesList.append(m3)
moviesList.append(m4)
moviesList.append(m5)
collectionView.register(CustomCollectionViewCell.self, forCellWithReuseIdentifier: CustomCollectionViewCell.identifier)
collectionView.frame = view.bounds
view.addSubview(collectionView)
    setupSearchBar()
    let urlString = "https://imdb-api.com/en/API/Search/k_il0e4iky/star"
    guard let url = URL(string: urlString) else {return}
    URLSession.shared.dataTask(with: url) { (data,response,error) in
        DispatchQueue.main.async {
            if let error = error {
                print("Error")
                return
            }
            guard let data = data else {return}
            let someString = String(data: data, encoding: .utf8)
            print(someString ?? "No Data")
        }
    }.resume()

}
    private func setupSearchBar(){
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
    }

}
extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource {
    
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
return moviesList.count
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
let movie = moviesList[indexPath.row]
let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomCollectionViewCell.identifier, for: indexPath) as!CustomCollectionViewCell

cell.layer.cornerRadius = 5
cell.layer.borderWidth = 0.3




//cell.configure(name: "\(movie.movieName!)", price: "\(movie.moviePrice!)", image: UIImage(named: movie.movieImageName ?? "error")!)
    cell.movieName.text = "123"
    cell.myImageView.image = UIImage(named: "Titanic")
    cell.buyButton.addTarget(self, action: #selector(gotoSecond), for: .touchUpInside)
    
    
    return cell
    }
    
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    
    }

extension ViewController {
    @objc func gotoSecond () {
        let rootVC = SecondViewController()
        
      
       let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: true, completion: nil)
        
    }
    
}
extension ViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
    }
}




