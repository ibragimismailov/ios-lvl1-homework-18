//
//  Movies.swift
//  M16NNdM17
//
//  Created by Abraam on 28.12.2021.
//


import Foundation
import UIKit
class Student {
    var name:String?
    var surname:String?
    var age:Int?
    var height:Double?
    init() {
        
    }
    init(name:String,surname:String,age:Int,height:Double) {
        self.name = name
        self.surname = surname
        self.age = age
        self.height = height
        
    }
}


class Movies {
    var movieID:Int?
    var moviePrice:Double?
    var movieName:String?

    var movieImageName:String?
    
    init(movieID:Int,moviePrice:Double,movieName:String,movieImageName:String) {
        self.movieID = movieID
        self.moviePrice = moviePrice
        self.movieName = movieName
        self.movieImageName = movieImageName

    }
    
    
}
