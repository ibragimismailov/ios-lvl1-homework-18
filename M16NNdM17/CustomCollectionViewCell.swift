//
//  CustomCollectionViewCell.swift
//  M16ndM17
//
//  Created by Abraam on 23.12.2021.
//

import UIKit
import SnapKit

class CustomCollectionViewCell: UICollectionViewCell {
static let identifier = "CustomCollectionViewCell"

public let movieName:UILabel = {
let name = UILabel()
name.text = ""
name.numberOfLines = 0
name.font = .boldSystemFont(ofSize: 17)
name.textAlignment = .center
return name
}()
private let moviePrice:UILabel = {
let price = UILabel()
price.text = ""
price.textAlignment = .center
price.font = .systemFont(ofSize: 14)
return price
}()
    public var buyButton:Brutton = {
    let buyMovie = Brutton(title: "Add To Card")
        return buyMovie
}()

 
public let myImageView:UIImageView = {
let image = UIImageView()
image.image = UIImage(named: "brazil")
image.contentMode = .scaleAspectFit
return image
}()


override init(frame: CGRect) {
super.init(frame: frame)
contentView.addSubview(myImageView)
contentView.addSubview(movieName)
contentView.addSubview(moviePrice)
contentView.addSubview(buyButton)

}



required init?(coder: NSCoder) {
fatalError("init(coder:) has not been implemented")
}
  

override func layoutSubviews() {
super.layoutSubviews()

myImageView.snp.makeConstraints { make in
make.top.equalTo(5)
make.left.equalTo(5)
make.right.equalTo(-5)
make.bottom.equalTo(contentView.snp_bottomMargin).inset(140)

movieName.snp.makeConstraints { make in
make.top.equalTo(myImageView.snp_bottomMargin).offset(5)
make.right.equalTo(-10)
make.left.equalTo(10)
make.bottom.equalTo(myImageView.snp_bottomMargin).inset(-30)

moviePrice.snp.makeConstraints { make in
make.top.equalTo(movieName.snp_bottomMargin).offset(5)
make.right.equalTo(-10)
make.left.equalTo(10)
make.bottom.equalTo(myImageView.snp_bottomMargin).inset(-50)

buyButton.snp.makeConstraints { make in
make.top.equalTo(moviePrice.snp_bottomMargin).offset(40)
make.right.equalTo(-10)
make.left.equalTo(10)
make.bottom.equalTo(contentView.snp_bottomMargin).inset(20)
}
}

}
}
}
public func configure (name:String,price:String,image:UIImage){
movieName.text = name
moviePrice.text = price
myImageView.image = image

}

override func prepareForReuse() {
movieName.text = nil
moviePrice.text = nil
myImageView.image = nil
    
}

    

}


